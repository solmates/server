'use strict'

import co from 'co'
import mongoose from 'mongoose'
import connection from '../database/connect'
import process from 'process'
import authHelper from '../helpers/auth'

const User = mongoose.model('User')

if (process.env.NODE_ENV === 'production') {
  console.error('ERROR:\n"NODE_ENV" in set to "production". You cannot add tokens to the production database!')
  process.exit(0)
}

co(function * () {
  try {
    var email = process.argv[2]
    if (!email) {
      console.error('[Error]: Email argument required.\nUsage:\nnode access-token my@email.com')
      process.exit(1)
    }

    yield connection.connect()
    console.log(email)
    var user = yield User.findOne({email: email})
    if (!user) {
      connection.disconnect()
      console.error('[Error]: Email address not found in the database, Token not generated!')
      process.exit(1)
    }

    var token = authHelper.createToken()
    yield authHelper.addTokenToUser(token, user._id)
    console.log('Access Token: ' + token.token)
    connection.disconnect()
    process.exit(0)
  } catch (err) {
    connection.disconnect()
    console.error(err)
    process.exit(1)
  }
})
