'use strict'

import co from 'co'
import program from 'commander'
import moment from 'moment'
import connection from '../database/connect'
import OneTimePin from 'OneTimePin'
import PrivateLocation from 'PrivateLocation'
import otpGenerator from 'otp-generator'
import process from 'process'

co(function * () {
  var location
  yield connection.connect()
  program
    .option('-d, --date <date>', 'Expiry date, in the format "mm/dd/yyyy"', /\d\d\/\d\d\/\d\d\d\d/i)
    .arguments('location')
    .action(function (locationVal) {
      location = locationVal
    })
    .parse(process.argv)

  console.log(location)
  if (!location) {
    console.error('Error: location is required')
    process.exit(1)
  }
  var privateLocation = yield PrivateLocation.findOne({LC: location})
  if (!privateLocation) {
    console.error('Error: location needs to be a private location')
    process.exit(1)
  }

  var key = otpGenerator.generate(6, {alphabets: false, upperCase: false, specialChars: false})

  var date
  if (typeof program.date === 'string') {
    date = moment(program.date, 'MM/DD/YYYY')
  } else {
    date = moment().add(7, 'days')
  }

  var otp = {
    key: key,
    date: date,
    LC: location
  }
  var otpInstance = new OneTimePin(otp)
  yield otpInstance.save()
  yield connection.disconnect()
  console.log(otpInstance)
})
