import express from 'express'
const app = express()

import bodyParser from 'body-parser'
import openapi from 'express-openapi'
import cors from 'cors'
import connection from './database/connect'
import co from 'co'
import path from 'path'

import apiDoc20 from './api-v2/api-doc20.js'
import apiDoc21 from './api-v2/api-doc21.js'

// export default app

co(function * () {
  try {
    if (!process.env.swagger_mockMode) {
      yield connection.connect()
    }

    var whitelist = ['http://localhost:8080', 'http://localhost']
    var corsOptions = {
      origin: function (origin, callback) {
        var originIsWhitelisted = whitelist.indexOf(origin) !== -1
        callback(null, originIsWhitelisted)
      }
    }

    app.use(cors({
      credentials: true,
      origin: corsOptions
    }))

    app.use(bodyParser.json())

    openapi.initialize({
      apiDoc: apiDoc20,
      app: app,
      paths: path.resolve(__dirname, 'api-v2', 'api-routes')
    })

    openapi.initialize({
      apiDoc: apiDoc21,
      app: app,
      paths: path.resolve(__dirname, 'api-v2', 'api-routes')
    })

    app.use(function (err, req, res, next) {
      res.status(err.status).json(err)
    })

    var port = process.env.PORT || 3000
    app.listen(port)

  // yield connection.sleep(1000)
  } catch (err) {
    console.error(err)
  }
})
