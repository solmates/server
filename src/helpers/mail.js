import config from 'config'
import logger from './logger'

import mailcomposer from 'mailcomposer'
import mailgunJS from 'mailgun-js'
const mailgun = mailgunJS({apiKey: config.get('mailgun.secret_key'),
domain: config.get('mailgun.domain')})

export default function (params) {
  return new Promise(function (resolve, reject) {
    if (config.get('mailgun.mode') !== 'live') {
      resolve()
      return
    }

    var to, cc
    Array.isArray(params.to) ? to = params.to.join(',') : to = params.to
    Array.isArray(params.cc) ? cc = params.cc.join(',') : cc = params.cc

    var mail = mailcomposer({
      from: 'SOLMATES <no-reply@getsolmates.com>',
      to: to,
      cc: cc,
      subject: params.subject,
      html: params.html
    })

    mail.build(function (mailBuildError, message) {
      var dataToSend = {
        to: to,
        message: message.toString('ascii')
      }

      mailgun.messages().sendMime(dataToSend, function (error, body) {
        if (error) {
          logger.error(error)
          reject(error)
        }
        resolve(body)
      })
    })
  })
}
