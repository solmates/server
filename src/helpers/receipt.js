import _ from 'underscore'
import co from 'co'
import fs from 'fs'
import Handlebars from 'handlebars'
import logger from './logger'
import moment from 'moment-timezone'
import pluralize from 'pluralize'

import sprintfJS from 'sprintf-js'
const sprintf = sprintfJS.sprintf

export default function (params) {
  return new Promise(function (resolve, reject) {
    co(function * () {
      var data = yield mapReceiptData(params)
      var html = yield htmlTemplate(data)
      var text = yield shortTemplate(data)
      resolve({html: html, short: text})
    })
  })
}

var mapReceiptData = function (params) {
  return new Promise(function promise (resolve, reject) {
    var location = params.location
    var payment = params.payment
    var provider = params.provider
    var reservation = params.reservation
    var products = params.products
    var latLng = location.gps.lat + ',' + location.gps.lon
    var mapUrl = 'https://maps.googleapis.com/maps/api/staticmap?center=' + latLng + '&zoom=16&markers=' + latLng + '&size=400x200'

    // Not the most effecient code, but works for now
    _.each(reservation.invoice.products, function (product, index) {
      var curProduct = _.findWhere(products, {SKU: product.SKU})
      reservation.invoice.products[index].description = curProduct.description
    })

    // This assumes there is at least one product, and that the time for all products is the same :(
    var timeunit = pluralize(products[0].billingPeriod, reservation.invoice.duration)
    var data = {
      id: reservation._id.toString(),
      firstname: reservation.guest.name.firstname,
      lastname: reservation.guest.name.lastname,
      tip: (reservation.tip / 100).toFixed(2),
      subTotal: (reservation.invoice.subtotal / 100).toFixed(2),
      tax: (reservation.invoice.tax / 100).toFixed(2),
      total: ((reservation.invoice.total + reservation.tip) / 100).toFixed(2),
      lastFour: payment.source.last4,
      location: location.description,
      address: location.address,
      date: moment(reservation.invoice.date).tz(location.timezone).format('MM/DD/YYYY'),
      time: moment(reservation.invoice.date).tz(location.timezone).format('h:mm a'),
      duration: reservation.invoice.duration,
      timeunit: timeunit,
      products: reservation.invoice.products,
      mapUrl: mapUrl,
      solmatesEmail: 'support@getsolmates.com',
      providerDescription: provider.description,
      providerContactName: provider.contact.name,
      providerTel: provider.contact.tel,
      providerEmail: provider.contact.email
    }
    resolve(data)
  })
}

var shortTemplate = function (data) {
  return new Promise(function promise (resolve, reject) {
    // For now this is hardcoded
    var textBody = sprintf('Reservation\n------------\nName: %s %s\nDate: %s %s\nProducts\n------\n',
      data.firstname, data.lastname, data.date, data.time)

    for (var i in data.products) {
      var product = data.products[i]
      textBody += sprintf('%d: %s\n', product.quantity, product.description)
    }
    resolve(textBody)
  })
}

var htmlTemplate = function (data) {
  return new Promise(function promise (resolve, reject) {
    try {
      var source = fs.readFileSync('./templates/receipt.hbs')
      var template = Handlebars.compile(source.toString())
      Handlebars.registerPartial('myPartial', '{{name}}')
      resolve(template(data))
    } catch (err) {
      reject(err)
      logger.error(err)
    }
  })
}
