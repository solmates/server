import bunyan from 'bunyan'
import config from 'config'

const logger = bunyan.createLogger(config.get('logger'))

logger.stream = {
  write: function (message, encoding) {
    logger.info(message)
  }
}

export default logger
