import config from 'config'
import redis from 'redis'
const redisClient = redis.createClient({
  host: config.get('redis.host'),
  port: config.get('redis.port')
})

export default {
  get: function (key) {
    return new Promise(function promise (resolve, reject) {
      redisClient.get(key, function (err, reply) {
        if (err) {
          reject(err)
          return
        }
        resolve(JSON.parse(reply))
      })
    })
  },

  set: function (key, value, ttl) {
    if (!ttl) { ttl = 3600 }
    var strValue = JSON.stringify(value)
    console.log(key)
    console.log(strValue)
    redisClient.set(key, strValue)
    redisClient.EXPIRE(key, ttl)
  }
}
