'use strict'

import co from 'co'
import config from 'config'
import logger from './logger'
import mongoose from 'mongoose'

import stripeFunc from 'stripe'
const stripe = stripeFunc(config.get('stripe.api_key'))

const Token = mongoose.model('Token')
const User = mongoose.model('User')

export default {
  transOAuth2Solmates: function (oauthUser) {
    console.log(oauthUser)
    var solmatesUser = {name: {}}
    solmatesUser['name']['firstname'] = oauthUser.firstname
    solmatesUser['name']['lastname'] = oauthUser.lastname
    solmatesUser['email'] = oauthUser.email

    console.log(solmatesUser)
    return solmatesUser
  },

  getUser: function (token) {
    return new Promise(function promise (resolve, reject) {
      co(function * () {
        try {
          var tokenDoc = yield Token.findOne({'tokens.token': token})
          if (!token || !tokenDoc) {
            // TODO: Not sure if its the right thing to reject the promise if the user is not found. Maybe some time we will want to test for that. Then we should probably wrap this fuction in a higher level function that rejects the promise if tokenDoc is undefined

            reject(new Error('Invalid Token!'))
          } else {
            var user = yield User.findById(tokenDoc.user)
            resolve(user)
          }
        } catch (error) {
          reject(error)
        }
      })
    })
  },

  getOrAddUser: function (me) {
    return new Promise(function (resolve, reject) {
      co(function * () {
        try {
          logger.error(me)
          var user = yield User.findOne({email: me.email})
          logger.error(user)
          if (user) {
            // We have a repeat login.
            if (!user.stripe) {
              let stripedUser = yield this.addUserToStripe(user)
              resolve(stripedUser)
            } else {
              // No updates neccessary to user
              resolve(user)
            }
          } else {
            // We have a new user
            let newUser = yield User.create(this.transOAuth2Solmates(me))
            let stripedUser = yield this.addUserToStripe(newUser)
            resolve(stripedUser)
          }
        } catch (err) {
          reject(err)
        }
      }.bind(this))
    }.bind(this))
  },

  addUserToStripe: function (user) {
    return new Promise(function (resolve, reject) {
      co(function * () {
        try {
          var customer = yield stripe.customers.create({description: 'Customer for ' + user.email})
          var stripeId = {stripe: customer.id}
          yield User.update({_id: mongoose.Types.ObjectId(user._id)}, stripeId)
          // For a long time I thought update would return the new user, but it doesnt... lets find him
          var newUser = yield User.findById(user._id)
          resolve(newUser)
        } catch (err) {
          reject(err)
        }
      })
    })
  }
}
