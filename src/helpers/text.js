import logger from './logger'

import config from 'config'
const accountSid = config.get('twilio.accountSid')
const authToken = config.get('twilio.authToken')

import twilio from 'twilio'
const client = twilio(accountSid, authToken)

export default function (to, body) {
  return new Promise(function (resolve, reject) {
    // For now we own only one number, could parameterize in future!
    var from = config.get('twilio.fromNumber')

    client.sendMessage({
      to: to,
      from: from,
      body: body
    })
      .then(function (message) {
        logger.info('message sent!')
        resolve(message)
      })
      .catch(function (error) {
        logger.error(error)
        reject(error)
      })
  })
}
