'use strict'

import co from 'co'
import config from 'config'
import logger from './logger'
import request from 'request'
import moment from 'moment'

import mongoose from 'mongoose'
const Token = mongoose.model('Token')

import hat from 'hat'
const rack = hat.rack()

// import redis from 'redis'
// const redisClient = redis.createClient()

export default {
  auth: function (token, provider) {
    return new Promise(function promise (resolve, reject) {
      var options = {
        url: 'https://oauth.io/auth/' + provider + '/me',
        headers: {'oauthio': 'k=' + config.get('oauthio.api_key') + '&access_token=' + token}
      }

      request.get(options, function (error, response, body) {
        if (error) {
          logger.error(error)
          reject(error)
        // res.status(403).send('Could not authenticate with the given oauth token.').end()
        } else {
          resolve(JSON.parse(body).data)
        }
      })
    })
  },

  createToken: function () {
    return {
      token: rack(),
      createDate: moment()
    }
  },

  addTokenToUser: function (token, userId) {
    return new Promise(function promise (resolve, reject) {
      co(function * () {
        try {
          var tokenDoc = yield Token.findOne({user: userId})
          if (!tokenDoc) {
            var newToken = new Token({user: userId, tokens: [token]})
            yield newToken.save()
            resolve()
          } else {
            tokenDoc.tokens.push(token)
            yield tokenDoc.save()
            resolve()
          }
        } catch (error) {
          reject(error)
        }
      })
    })
  },

  getToken: function (authorizationHeader) {
    return new Promise(function (resolve, reject) {
      var myRe = /Bearer (.*)/g
      var myArray = myRe.exec(authorizationHeader)
      if (myArray !== null && myArray.length > 1) {
        resolve(myArray[1])
      } else {
        resolve(undefined)
      }
    })
  }
}
