import logger from './logger'

export default {
  straight: function (parameters, quantity, duration) {
    return new Promise(function promise (resolve, reject) {
      logger.debug(parameters)
      logger.debug(quantity)
      logger.debug(duration)
      var price = parameters.price
      var total = price * quantity * duration
      resolve(total)
    })
  },
  timeDiscount: function (parameters, quantity, duration) {
    return new Promise(function promise (resolve, reject) {
      logger.debug(parameters)
      logger.debug(quantity)
      logger.debug(duration)
      var priceKeys = Object.keys(parameters.dailyRate)
      var range

      logger.debug(priceKeys)
      for (var i in priceKeys) {
        if (parseInt(priceKeys[i]) <= duration) {
          range = priceKeys[i]
        }
      }

      var price = parameters.dailyRate[range]
      var total = price * quantity * range

      var additional = duration - range
      if (parameters.additional && additional > 0) {
        total += parameters.additional * quantity * additional
      }

      resolve(total)
    })
  }

}
