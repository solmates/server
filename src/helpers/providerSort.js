import co from 'co'
import config from 'config'

const list = 'providerList'

var redis = require('redis').createClient({
  host: config.get('redis.host'),
  port: config.get('redis.port')
})

export default {
  roundRobin: function (providers) {
    return new Promise(function (resolve, reject) {
      co(function * () {
        var provider = yield findFirstProvider(providers)
        yield lRemove(provider)
        yield rPush(provider)
        var cache = yield this.getCache()
        resolve(cache)
      }.bind(this))
    }.bind(this))
  },

  initialize: function (providers) {
    return new Promise(function promise (resolve, reject) {
      co(function * () {
        this.providers = providers
        yield cleanList()
        redis.lpush(list, providers, function (err, reply) {
          if (err) {
            reject(err)
            return
          }
          resolve(reply)
        })
      }.bind(this))
    }.bind(this))
  },

  getCache: function () {
    return new Promise(function promise (resolve, reject) {
      co(function * () {
        var length = yield getLength()
        redis.lrange(list, 0, length, function (err, reply) {
          if (err) {
            reject(err)
            return
          }
          resolve(reply)
        })
      })
    })
  }
}

var getLength = function () {
  return new Promise(function promise (resolve, reject) {
    redis.llen(list, function (err, reply) {
      if (err) {
        reject(err)
        return
      }
      resolve(reply)
    })
  })
}

var cleanList = function () {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      redis.del(list, function (err, reply) {
        if (err) {
          reject(err)
          return
        }
        resolve(reply)
      })
    })
  })
}

var rPush = function (provider) {
  return new Promise(function promise (resolve, reject) {
    redis.rpush(list, provider, function (err, reply) {
      if (err) {
        reject(err)
        return
      }
      resolve(reply)
    })
  })
}

var lRemove = function (provider) {
  return new Promise(function promise (resolve, reject) {
    redis.lrem(list, 1, provider, function (err, reply) {
      if (err) {
        reject(err)
        return
      }
      resolve(reply)
    })
  })
}

var lIndex = function (index) {
  return new Promise(function promise (resolve, reject) {
    redis.lindex(list, index, function (err, reply) {
      if (err) {
        reject(err)
        return
      }
      resolve(reply)
    })
  })
}

var findFirstProvider = function (availableProviders) {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      var length = yield getLength()
      var i = 0
      var found = false
      while (!found && (i < length)) {
        var value = yield lIndex(i)
        if (availableProviders.indexOf(value) >= 0) {
          found = value
          resolve(value)
        }
        i++
      }
    })
  })
}
