'use strict'

import _ from 'underscore'
import co from 'co'
import moment from 'moment'
import 'moment-round'

import quoteAlgs from './quoteAlgorithms'
import quoteCache from './quoteCache'
import logger from './logger'

import mongoose from 'mongoose'
const Location = mongoose.model('Location')
const Pricelist = mongoose.model('Pricelist')

import hat from 'hat'
const rack = hat.rack()

export default function (quote, quoteId) {
  return new Promise(function (resolve, reject) {
    co(function * () {
      var subtotal = 0
      var quoteResponse = JSON.parse(JSON.stringify(quote))

      var location = yield Location.findOne({LC: quote.LC})
      if (!location) {
        reject(Error('Unknown Location Code. Could not generate a quote!'))
        return
      }

      if (!quote.products || quote.products.length === 0) {
        reject(Error('No product specified. Could not generate a quote!'))
        return
      }
      var notification
      logger.debug(_.pluck(quote.products, 'SKU'))
      var pricelists = yield Pricelist.find({PC: quote.PC, SKU: {$in: _.pluck(quote.products, 'SKU')}})
      if (!pricelists) {
        reject(Error('Unknown Provider Code or SKU. Could not generate a quote!'))
        return
      }
      for (var i in quote.products) {
        logger.debug(quote.products[i])
        var pricelist = _.findWhere(pricelists, {SKU: quote.products[i].SKU})
        logger.debug(pricelist)
        // var pricelist = yield Pricelist.findOne({PC: quote.PC, SKU: quote.products[i].SKU})
        var price = yield quoteAlgs[pricelist.strategy](pricelist.parameters, quote.products[i].quantity, quote.products[i].duration)
        logger.debug(price)
        quoteResponse.products[i].price = price
        subtotal += price
        if (pricelist.notification) {
          notification = pricelist.notification
        }
        // Calculate the next available date
        var nextDate = getNextAvailableDate(pricelist.leadTime, location.timezone)
        // if no date is given, set the next available date
        if (!quote.date) {
          quoteResponse.date = nextDate
        } else {
          // if no date is given, check that it is valid
          var quoteDate = moment(quote.date)
          if (quoteDate.isBefore(nextDate)) {
            reject(Error('The given date is invalid! The next available date is: ' + nextDate.format('MM/DD h:mm a')))
          }
        }
      }

      var tax = subtotal * location.taxRate
      var total = subtotal + tax

      // round the quote responses, otherwise it might result in non integer results
      quoteResponse.subtotal = Math.round(subtotal)
      quoteResponse.tax = Math.round(tax)
      quoteResponse.total = Math.round(total)

      if (notification) {
        quoteResponse.notification = notification
      } else {
        quoteResponse.notification = undefined
      }

      var id
      ;(!quoteId) ? id = rack() : id = quoteId
      quoteResponse.quoteId = id

      quoteResponse.duration = quote.products[0].duration
      quoteCache.set(id, quoteResponse)
      logger.debug(quoteResponse)
      resolve(quoteResponse)
    })
  })
}

var getNextAvailableDate = function (leadTime, timezone) {
  // if no leadtime is specified, assume 60 minutes
  if (!leadTime) { leadTime = 60 }

  var nextDate = moment().tz(timezone)
  nextDate.add(leadTime, 'minutes')
  nextDate.round(15, 'minutes')

  // if the time is beyond 19:00... set the date to the next day 9:00
  if (parseInt(nextDate.format('H')) >= 19) {
    nextDate.add(1, 'days')
    nextDate.hours(9)
    nextDate.minutes(0)
  }

  return nextDate
}
