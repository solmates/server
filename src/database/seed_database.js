'use strict'

import co from 'co'
import mongoose from 'mongoose'
import connection from './connect'

if (process.env.NODE_ENV === 'production') {
  console.error('ERROR:\n"NODE_ENV" in set to "production". You cannot seed the production database!')
  process.exit(0)
}

co(function * () {
  yield connection.connect()

  try {
    // Regions
    var Region = mongoose.model('Region')
    const regions = require('dummy-data/data/regions.js')
    for (let i in regions) {
      yield Region.create(regions[i])
    }

    let Product = mongoose.model('Product')
    const products = require('dummy-data/data/products.js')
    for (let i in products) {
      yield Product.create(products[i])
    }

    // Lets change pace and set up the user and providers
    let User = mongoose.model('User')
    const users = require('dummy-data/data/users.js')
    for (let i in users) {
      yield User.create(users[i])
    }

    let Provider = mongoose.model('Provider')
    const providers = require('dummy-data/data/providers.js')
    for (let i in providers) {
      yield Provider.create(providers[i])
    }

    let Pricelist = mongoose.model('Pricelist')
    const pricelists = require('dummy-data/data/pricelists.js')
    for (let i in pricelists) {
      yield Pricelist.create(pricelists[i])
    }

    // Locations
    let Location = mongoose.model('Location')
    const locations = require('dummy-data/data/locations.js')
    for (let i in locations) {
      yield Location.create(locations[i])
    }

    // Private Locations
    let PrivateLocation = mongoose.model('PrivateLocation')
    const privateLocations = require('dummy-data/data/privateLocations.js')
    for (let i in privateLocations) {
      yield PrivateLocation.create(privateLocations[i])
    }

  // // TODO: Leave reservations out for now, lets come back to it when we need it
  // // Create some dummy reservations
  // var Reservation = mongoose.model('Reservation')
  // const reservations = require('dummy-data/data/reservations.js')
  // console.log(reservations)
  // for (let i in reservations) {
  //   var reservationObject = reservations[i]
  //   let locations = yield Location.find({})
  //   var newLocation = locations[i % locations.length]
  //   Object.assign(reservationObject, {'location': {'LC': newLocation.LC}})
  //   Object.assign(reservationObject, {'products': {'SKU': 'beachset', 'quantity': 1}})
  //   // get a date some time in the next week
  //   var date = 24 * ((i % 5) + 1)
  //   Object.assign(reservationObject, {'date': moment().hours(date)})
  //   console.log(reservationObject)
  //   yield Reservation.create(reservationObject)
  // }
  } catch (err) {
    console.log(err)
  }

  connection.disconnect()
})
