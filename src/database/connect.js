import config from 'config'
import mongoose from '../helpers/mongoose'
import path from 'path'

// TODO: FIX THIS!
require('require-all')({dirname: path.join(__dirname, 'models')})

export default {
  connect: function () {
    var url = this.connectionURL()
    return mongoose.connect(url)
  },

  disconnect: function () {
    return mongoose.connection.close()
  },

  connectionURL: function () {
    var userString = ''
    if (config.has('dbConfig.username') && config.has('dbConfig.password')) {
      userString = config.get('dbConfig.username') + ':' + config.get('dbConfig.password') + '@'
    }
    var connectURL = ['mongodb://', userString, config.get('dbConfig.host'), ':',
      config.get('dbConfig.port'), '/', config.get('dbConfig.dbName')].join('')
    return connectURL
  }
}

// Some events to let the user know what is happening

process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination')
    process.exit(0)
  })
})

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection connected')
})

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected')
})

mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err)
})
