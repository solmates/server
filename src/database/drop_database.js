import mongoose from 'mongoose'
import connection from './connect'
import co from 'co'

if (process.env.NODE_ENV === 'production') {
  console.error('ERROR:\n"NODE_ENV" in set to "production". You cannot drop the production database!')
  process.exit(0)
}

co(function * () {
  yield connection.connect()
  yield mongoose.connection.db.dropDatabase()
  yield connection.disconnect()
})
