import mongoose from 'mongoose'
import Product from './product'

export default mongoose.model('Agenda',
  mongoose.Schema({
    name: String,
    description: String,
    productss: [{type: mongoose.Schema.Types.ObjectId, ref: Product.schema}]
  }))
