import mongoose from 'mongoose'

export default mongoose.model('PrivateLocation',
  mongoose.model('Location').schema
)
