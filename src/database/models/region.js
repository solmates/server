import mongoose from 'mongoose'

export default mongoose.model('Region',
  mongoose.Schema({
    RC: {type: String, required: [true, 'No region code?']},
    name: String,
    paths: []
  }))
