import mongoose from 'mongoose'

export default mongoose.model('Provider',
  mongoose.Schema({
    PC: String,
    name: String,
    description: String,
    admins: [{email: String}],
    contact: {
      name: String,
      tel: String,
      email: String,
      url: String
    }
  })
)
