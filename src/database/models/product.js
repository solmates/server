import mongoose from 'mongoose'

export default mongoose.model('Product',
  mongoose.Schema({
    SKU: {type: String, required: [true, 'No product SKU?']},
    description: {type: String, required: [true, 'No product description?']},
    children: [
      {
        SKU: {type: String, required: [true, 'No child SKU?']},
        quantity: {type: Number, required: [true, 'No child quantity?']}
      }],
    showFooter: Boolean,
    billingPeriod: String,
    unavailable: Boolean,
    addon: Boolean
  }))
