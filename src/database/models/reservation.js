import mongoose from 'mongoose'

var invoiceSchema = mongoose.Schema({
  quoteId: {type: String, required: [true, 'No invoice.quoteId?']},
  PC: {type: String, required: [true, 'No invoice.PC?']},
  LC: {type: String, required: [true, 'No invoice.LC?']},
  date: {type: Date, required: [true, 'No invoice.Date?']},
  duration: {type: Number, required: [true, 'No invoice.duration?']},
  products: [{
    SKU: {type: String, required: [true, 'No invoice.product?']},
    quantity: {type: Number, required: [true, 'No invoice.quantity?']},
    price: {type: Number, required: [true, 'No invoice.price?']}
  }],
  subtotal: {type: Number, required: [true, 'No invoice.subtotal?']},
  tax: {type: Number, required: [true, 'No invoice.tax?']},
  total: {type: Number, required: [true, 'No invoice.total?']}
})

export default mongoose.model('Reservation',
  mongoose.Schema({
    tip: Number,
    invoice: invoiceSchema,
    guest: {
      name: {
        firstname: {type: String, required: [true, 'No invoice.guest.name.firstname?']},
        lastname: {type: String, required: [true, 'No invoice.guest.name.lastname?']}
      },
      email: {type: String, required: [true, 'No invoice.guest.email?']}
    },
    user: {email: String}
  })
)
