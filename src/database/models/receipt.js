import mongoose from 'mongoose'
import Reservation from './reservation'

export default mongoose.model('Receipt',
  mongoose.Schema({
    reservation: {type: mongoose.Schema.Types.ObjectId, ref: Reservation.schema},
    receipt: String,
    user: {email: String}
  })
)
