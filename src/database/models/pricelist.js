import mongoose from 'mongoose'

export default mongoose.model('Pricelist',
  mongoose.Schema({
    PC: {type: String, required: [true, 'No Product Code?']},
    SKU: {type: String, required: [true, 'No SKU?']},
    strategy: {type: String, required: [true, 'No Strategy?']},
    parameters: {type: Object},
    leadTime: Number,
    locations: [{LC: {type: String, required: [true, 'No Location Code?']}}],
    notification: String,
    minTime: Number,
    maxTime: Number
  }, {strict: false}))
