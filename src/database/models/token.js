import mongoose from 'mongoose'
import User from './user'

export default mongoose.model('Token',
  {
    user: {type: mongoose.Schema.Types.ObjectId, ref: User.schema},
    tokens: [{
      token: String,
      createDate: Date
    }]
  })
