import mongoose from 'mongoose'
const OneTimePin = mongoose.model('OneTimePin').schema

module.exports = mongoose.model('User',
  {
    name: {
      firstname: {type: String, required: [true, 'No first name?']},
      lastname: {type: String, required: [true, 'No last name?']}
    },
    email: {type: String, required: [true, 'No email?']},
    stripe: String,
    privateZones: [OneTimePin]
  })
