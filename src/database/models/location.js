import mongoose from 'mongoose'

export default mongoose.model('Location',
  mongoose.Schema({
    LC: {type: String, required: [true, 'No location code?']},
    address: {type: String, required: [true, 'No address?']},
    description: {type: String, required: [true, 'No location description?']},
    gps: {type: {lng: Number, lat: Number}, required: [true, 'No GPS coordinates?']},
    private: Boolean,
    products: [String],
    state: {type: String, required: [true, 'No state/province?']},
    taxRate: {type: Number, required: [true, 'No tax rate?']},
    timezone: {type: String, required: [true, 'No time zone?']}
  }))
