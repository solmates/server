import mongoose from 'mongoose'

export default mongoose.model('OneTimePin',
  mongoose.Schema({
    key: {type: String, required: [true, 'No key?']},
    date: {type: Date, required: [true, 'No exiry date?']},
    LC: {type: String, required: [true, 'No location?']}
  }))
