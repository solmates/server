/**
 * @deprecated Since version 2.1
 */
export default {
  properties: {
    _id: {
      type: 'string',
      format: 'binary'
    },
    name: {
      type: 'string'
    },
    paths: {
      type: 'array',
      items: {
        type: 'array',
        items: {
          type: 'number'
        }
      }
    }
  }
}
