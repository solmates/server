const request = {
  properties: {
    date: {
      description: 'The agenda date',
      type: 'string',
      format: 'date'
    },
    agenda: {
      description: 'Object wrapping the agenda details',
      type: 'object',
      properties: {
        totals: {
          $ref: '#/definitions/totals'
        },
        locations: {
          'description': 'The agenda date',
          type: 'array',
          items: {
            $ref: '#/definitions/location'
          }
        },
        product: {
          $ref: '#/definitions/product'
        },
        reservations: {
          type: 'array',
          items: {
            $ref: '#/definitions/reservation'
          }
        },
        items: {
          type: 'array',
          items: {
            $ref: '#/definitions/item'
          }
        }
      }
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
