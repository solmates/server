const request = {
  required: ['name', 'email'],
  properties: {
    name: {
      type: 'object',
      required: ['firstname', 'lastname'],
      properties: {
        firstname: {
          description: 'The first name',
          type: 'string'
        },
        lastname: {
          description: 'The last name',
          type: 'string'
        }
      }
    },
    email: {
      description: 'The email address',
      type: 'string'
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
