const request = {
  required: ['guest', 'quoteId', 'paymentId', 'tipPercent'],
  properties: {
    guest: {
      description: 'The guest',
      type: 'object'
    },
    quoteId: {
      description: 'Quote id, retrieved from POST /quote',
      type: 'string'
    },
    paymentId: {
      description: 'Stripe payment id',
      type: 'string'
    },
    tipPercent: {
      description: 'The tip percentage',
      type: 'number'
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
