const request = {
  properties: {
    // TODO: should we even move PC into products to be able to have different providers in one quote? That will cause some extra admin, so for now you have to make a quote for each provider. 
    PC: {
      description: 'The Provider Code for the provider that is to fulfill the quote',
      type: 'string'
    },

    LC: {
      description: 'The Location Code for the location of the quote',
      type: 'string'
    },

    date: {
      description: 'The date for the quote. If omitted, the first available time will be set.',
      type: 'string',
      format: 'date-time'
    },

    // guest: {
    //   description: 'The guest',
    //   type: 'object',
    //   $ref: '#/definitions/guest'
    // },

    products: {
      description: 'The quote products',
      type: 'array',
      minItems: 1,
      uniqueItems: true,
      items: {
        type: 'object',
        properties: {
          SKU: {
            description: "The product's Stock Taking Unit",
            type: 'string'
          },
          quantity: {
            description: 'The quantity of the given product',
            type: 'number'
          },
          duration: {
            description: "The time to rent this product (measured in the product's native unit)",
            type: 'number'
          }
        },
        required: ['SKU', 'quantity', 'duration']
      }
    }
  },
  required: ['PC', 'LC', 'products']
}

const response = {
  properties: {
    quoteId: {
      description: 'Quote Identifier',
      type: 'string'
    },
    PC: {
      description: 'The Provider Code that is to fulfill the quote',
      type: 'string'
    },

    LC: {
      description: 'The Location Code for the location of the quote',
      type: 'string'
    },

    date: {
      description: 'The date for the quote',
      type: 'string',
      format: 'date-time'
    },

    // guest: {
    //   description: 'The guest',
    //   type: 'object',
    //   $ref: '#/definitions/guest'
    // },

    products: {
      description: "The product's Stock Taking Unit",
      type: 'array',
      minItems: 1,
      uniqueItems: true,
      items: {
        type: 'object',
        properties: {
          SKU: {
            description: "The product's Stock Taking Unit",
            type: 'string'
          },
          quantity: {
            description: 'The quantity of the given product',
            type: 'number'
          },
          duration: {
            description: "The time to rent this product (measured in the product's native unit)",
            type: 'number'
          },
          price: {
            description: 'The quoted product price, excluding tax',
            type: 'number'
          }
        },
        required: ['SKU', 'quantity', 'duration']
      }
    },
    tax: {
      description: 'The quote tax.',
      type: 'number'
    },
    total: {
      description: 'The quote total, including tax',
      type: 'number'
    }
  },
  required: ['quoteId', 'PC', 'LC', 'date', 'products', 'tax', 'total']
}

export { request, response }
