const request = {
  properties: {
    customer: {
      type: 'object'
    },
    source: {
      type: 'object'
    },
    metadata: {
      type: 'object'
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
