const request = {
  required: [
    '_id',
    'name',
    'gps',
    'products'
  ],
  properties: {
    _id: {
      type: 'string'
    },
    name: {
      description: 'Short name for the location',
      type: 'string'
    },
    description: {
      description: 'A longer description of the location',
      type: 'string'
    },
    company: {
      description: 'The name of the company operating at the location',
      type: 'string'
    },
    state: {
      description: 'A two character US state code',
      type: 'string'
    },
    gps: {
      description: 'The GPS coordinates for the location',
      $ref: '#/definitions/gps'
    },
    products: {
      description: "An array of item id's, for items available at the location",
      type: 'array',
      items: {
        type: 'string'
      }
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
