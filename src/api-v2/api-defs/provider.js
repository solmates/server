const request = {
  properties: {
    PC: {type: 'string'}, description: {type: 'string'},
    contact: {type: 'object',
      properties: {
        name: {description: "The provider's name", type: 'string'},
        tel: {description: "The provider's telephone number", type: 'string'},
        email: {description: "The provider's email address", type: 'string'},
        url: {description: "The provider's website", type: 'string'},
        admins: {description: 'A list of administrators for the provider', type: 'array',
          items: {type: 'object',
            properties: {
              email: {description: "The admin's email address", type: 'string'}
            },
            required: ['email']
          }
        }
      },
      required: ['name', 'tel', 'email', 'url', 'admins']

    }
  },
  required: ['PC', 'description', 'contact']

}

const response = {
  properties: {
  }
}

export { request, response }
