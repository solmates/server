const request = {
  required: ['name'],
  properties: {
    name: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    filterDisplay: {
      description: 'Should this item be displayed in the filter view?',
      type: 'boolean'
    },
    items: {
      description: "Array of item id's for this product",
      type: 'array',
      items: {
        type: 'string',
        format: 'binary'
      }
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
