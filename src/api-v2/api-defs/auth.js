const request = {
  required: ['token', 'provider'],
  properties: {
    token: {
      description: 'The authentication token returned by the provider',
      type: 'string'
    },
    provider: {
      description: 'The name of the provider used for authentication',
      type: 'string'
    }
  }
}

const response = {
  properties: {
  }
}

export { request, response }
