import { request as authRequest } from './api-defs/auth'
import { request as guestRequest } from './api-defs/guest'
import { response as locationResponse } from './api-defs/location'
import { response as productResponse } from './api-defs/product'
import { request as paymentRequest, response as paymentResponse } from './api-defs/payment'
import { request as providerRequest } from './api-defs/provider'
import { request as quoteRequest, response as quoteResponse } from './api-defs/quote'

export default {
  swagger: '2.0',

  // all routes will now have /v2 prefixed.
  basePath: '/v2.1',

  info: {
    title: 'Solmates Server',
    version: '2.1.0'
  },

  definitions: {
    authRequest: authRequest,
    guestRequest: guestRequest,
    locationResponse: locationResponse,
    productResponse: productResponse,
    paymentRequest: paymentRequest,
    paymentResponse: paymentResponse,
    providerRequest: providerRequest,
    quoteRequest: quoteRequest,
    quoteResponse: quoteResponse
  // region: region,
  // reservation: reservation
  },

  paths: {}
}
