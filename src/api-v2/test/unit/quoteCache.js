/* global describe, it */
var co = require('co') // eslint-disable-line no-unused-vars
var quoteCache = require('../../helpers/quoteCache')
var should = require('should') // eslint-disable-line no-unused-vars
var sleep = require('../sleep')
describe('Test quoteCache', function () {
  it('should add a quote to the cache', function (done) {
    co(function * () {
      try {
        quoteCache.set('foo', 'bar', 1)
        var cache = yield quoteCache.get('foo')
        should.equal(cache, 'bar')
        done()
      } catch (err) {
        done(err)
      }
    })
  })
  it('should return "null" when the cache have expired', function (done) {
    this.timeout(5000)
    co(function * () {
      try {
        // sleep until the cache has expired
        yield sleep(2000)
        var cache = yield quoteCache.get('foo')
        should.equal(cache, null)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
  it('should handle JSON data correctly', function (done) {
    co(function * () {
      try {
        quoteCache.set('fooey', {complex: 'data'}, 1)

        var cache = yield quoteCache.get('fooey')
        should.equal(cache.complex, 'data')
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
