module.exports = function (millis) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve()
    }, millis)
  })
}
