// /* global describe, it */

// var should = require('should') // eslint-disable-line no-unused-vars
// var request = require('supertest')
// var server = require('../../../app')
// var helperMethods = require('../helperMethods')
// var co = require('co')

// describe('GET /admin/locations', function () {
//   var nrOfLocations = 1
//   it('should have ' + nrOfLocations + ' locations', function (done) {
//     co(function * () {
//       var locations = yield helperMethods.getLocations()
//       yield helperMethods.createTestUser()
//       request(server)
//         .get('/admin/locations')
//         .set('Accept', 'application/json')
//         .expect('Content-Type', /json/)
//         .expect(200)
//         .end(function (err, res) {
//           should.not.exist(err)
//           res.body.should.have.lengthOf(nrOfLocations)
//           done()
//         })
//     })
//   })
// })
