/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var app = require('../../../app.js')
var agent = request.agent(app)
var paymentHelpers = require('../../helpers/paymentHelpers')
var dataHelpers = require('../../helpers/dataHelpers.js')

var ReceiptTest = function () {
  describe('GET /receipt/:id', function () {
    this.timeout(10000)
    it('should return the receipt for the given reservation', function (done) {
      var testObject = dataHelpers.createTestObject()
      dataHelpers.setUser(testObject, {name: {firstname: 'test', lastname: 'user'}})
      dataHelpers.setReservation(testObject, {sets: 2})

      paymentHelpers.makePayment(testObject)
        .then(function (paymentResult) {
          agent.get('/receipts/' + paymentResult.reservation._id)
            .set('authorization', 'Bearer ' + paymentResult.user.access_token)
            .end(function (err, res) {
              if (err) {
                done(err)
              } else {
                // TODO: Maybe we can devise a better test here?
                res.body.should.have.property('receipt')
                done()
              }
            })
        })
        .catch(function (error) {
          done(error)
        })
    })
  })
}

module.exports = ReceiptTest
