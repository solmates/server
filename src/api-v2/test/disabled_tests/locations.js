/* global describe, it */

// var should = require('should') // eslint-disable-line no-unused-vars
var locationHelpers = require('../../helpers/locationHelpers.js')
var dataHelpers = require('../../helpers/dataHelpers.js')

var LocationTest = function () {
  describe('GET /locations', function () {
    it('should have 9 locations', function (done) {
      locationHelpers.getLocations()
        .then(function (locationsResult) {
          locationsResult.locations.should.have.lengthOf(9)
          done()
        })
        .catch(function (error) {
          done(error)
        })
    })
  })

  describe('POST /locations', function () {
    it('should add a new location', function (done) {
      var testObject = dataHelpers.createTestObject()
      dataHelpers.setLocation(testObject, {
        name: 'Nashville Beach', state: 'TN', rate: 35,
        gps: {lon: -86.801908000, lat: 36.141266600}
      })

      locationHelpers.postLocation(testObject)
        .then(function (locationResult) {
          locationResult.location.should.have.property('name', 'Nashville Beach')
          done()
        })
        .catch(function (error) {
          done(error)
        })
    })
  })
}

module.exports = LocationTest
