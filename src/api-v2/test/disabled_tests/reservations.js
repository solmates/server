/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var reservationHelpers = require('../../helpers/reservationHelpers.js')
var dataHelpers = require('../../helpers/dataHelpers.js')

var ReservationTest = function () {
  describe('GET /reservations', function () {
    it('should have 1 reservations', function (done) {
      var testObject = dataHelpers.createTestObject()
      dataHelpers.setUser(testObject, {name: {firstname: 'test', lastname: 'user'}})
      dataHelpers.setReservation(testObject, {sets: 1})
      reservationHelpers.makeReservation(testObject)
        .then(function (makeReservationResult) {
          reservationHelpers.getReservations(makeReservationResult)
            .then(function (getReservationsResult) {
              getReservationsResult.reservations.should.have.lengthOf(1)
              // TODO: We could add more assertions here
              done()
            })
            .catch(function (error) {
              done(error)
            })
        })
        .catch(function (error) {
          done(error)
        })
    })
  })

  describe('POST /reservations', function () {
    it('should add a new reservation', function (done) {
      var testObject = dataHelpers.createTestObject()
      dataHelpers.setUser(testObject, {name: {firstname: 'test', lastname: 'user'}})
      dataHelpers.setReservation(testObject, {sets: 2})

      reservationHelpers.makeReservation(testObject)
        .then(function (reservationsResult) {
          reservationsResult.reservation.should.have.property('sets', 2)
          done()
        })
        .catch(function (error) {
          done(error)
        })
    })
  })
}

module.exports = ReservationTest
