/* global describe it */

var chai = require('chai') // eslint-disable-line no-unused-vars
// var chaiAsPromised = require('chai-as-promised')
// chai.use(chaiAsPromised)
// var expect = chai.expect

// should.use(chaiAsPromised)
// var assert = chai.assert
var requestHelper = require('../../../utils/requestHelper.js')
var paymentHelpers = require('../../helpers/paymentHelpers.js')
var userHelpers = require('../../helpers/userHelpers.js')
var voucherHelpers = require('../../helpers/voucherHelpers.js')

var VoucherTest = function () {
  describe('POST /vouchers/create', function () {
    // The important thing is the user creating the voucher must have admin privelages
    var user = {access: requestHelper.userType.admin}
    it('should create a voucher', function (done) {
      // expect(voucher).to.eventually.have.property('voucher')
      //     .to.have.property('value').and.equal(1009),
      // expect(voucher).to.eventually.have.property('voucher')
      //   .to.have.property('code').with.length(8)
      // return voucher.should.eventually.equal(4)
      // assert.eventually.equal(voucher, )
      // console.log(Promise.resolve(2 + 2).should)
      // Promise.resolve(2 + 2).should.eventually.equal(4)
      // console.log(voucher.should.eventually)
      // .should.eventually.equal(1000).notify(done)
      // console.log(voucher)
      // voucher.should.eventually.equal(1000)
      voucherHelpers.createVoucher(user, 1000)
        .then(function (voucherResult) {
          voucherResult.voucher.should.have.property('value', 1000)
          voucherResult.voucher.should.have.property('code').with.lengthOf(8)
          done()
        })
        .catch(function (error) {
          done(error)
        })
    })
  })

  describe('POST /vouchers/redeem', function () {
    it('should redeem a voucher', function (done) {
      var admin = {access: requestHelper.userType.admin}
      voucherHelpers.createVoucher(admin, 1000)
        .then(function (voucherResult) {
          var user = {access: requestHelper.userType.user}
          voucherHelpers.redeemVoucher(user, voucherResult.voucher)
            .then(function (redeemVoucherResult) {
              redeemVoucherResult.voucher.should.have.property('redeemed', true)
              done()
            })
            .catch(function (error) {
              done(error)
            })
        })
        .catch(function (error) {
          done(error)
        })
    })

    it('should fail to redeem a voucher twice', function (done) {
      var admin = {access: requestHelper.userType.admin}
      voucherHelpers.createVoucher(admin, 1000)
        .then(function (createVoucherResult) {
          createVoucherResult.voucher.should.have.property('redeemed', false)
          var user = {access: requestHelper.userType.user}
          var redeemVoucher = voucherHelpers.redeemVoucher(user, createVoucherResult.voucher)
          redeemVoucher.then(function (redeemVoucherResult) {
            userHelpers.getMe(redeemVoucherResult.user.access_token)
            redeemVoucherResult.voucher.should.have.property('redeemed', true)
            voucherHelpers.redeemVoucher(user, createVoucherResult.voucher)
              .then(function (redeemVoucher2Result) {
                done(new Error("we expected an error to occur here, but it didn't"))
              })
              .catch(function (error) {
                error.response.error.should.have.property('text', 'This voucher has already been redeemed!')
                done()
              })
          })
            .catch(function (error) {
              done(error)
            })
        })
        .catch(function (error) {
          done(error)
        })
    })

    it('should redeem two vouchers for one user', function (done) {
      var admin = {access: requestHelper.userType.admin}
      voucherHelpers.createVoucher(admin, 1000)
        .then(function (createVoucher1Result) {
          voucherHelpers.createVoucher(admin, 2000)
            .then(function (createVoucher2Result) {
              var user = {access: requestHelper.userType.user}
              voucherHelpers.redeemVoucher(user, createVoucher1Result.voucher)
                .then(function (redeemVoucher1Result) {
                  redeemVoucher1Result.voucher.should.have.property('redeemed', true)
                  userHelpers.getMe(redeemVoucher1Result.user.access_token)
                    .then(function (me) {
                      me.user.voucher.should.have.property('amount', 1000)
                      voucherHelpers.redeemVoucher(user, createVoucher2Result.voucher)
                        .then(function (redeemVoucher2Result) {
                          redeemVoucher2Result.voucher.should.have.property('redeemed', true)
                          userHelpers.getMe(redeemVoucher2Result.user.access_token)
                            .then(function (me2) {
                              me2.user.voucher.amount.should.be.exactly(2000).and.be.a.Number()
                              done()
                            })
                        })
                        .catch(function (error) {
                          done(error)
                        })
                    })
                })

                .catch(function (error) {
                  done(error)
                })
            })
            .catch(function (error) {
              done(error)
            })
        })
        .catch(function (error) {
          done(error)
        })
    })

    it('should redeem a voucher and process the transaction', function (done) {
      this.timeout(10000)
      var admin = {access: requestHelper.userType.admin}
      voucherHelpers.createVoucher(admin, 1000)
        .then(function (createVoucherResult) {
          var user = {access: requestHelper.userType.user}
          voucherHelpers.redeemVoucher(user, createVoucherResult.voucher)
            .then(function (redeemVoucherResult) {
              var reservation = {}
              paymentHelpers.makePayment(0, redeemVoucherResult.user, reservation)
                .then(function (paymentResult) {
                  paymentResult
                  done()
                })
                .catch(function (error) {
                  done(error)
                })
            })
            .catch(function (error) {
              done(error)
            })
        })
        .catch(function (error) {
          done(error)
        })
    })
  })
}

module.exports = VoucherTest
