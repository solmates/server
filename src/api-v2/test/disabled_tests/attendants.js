/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../../app')

// var userHelpers = require('../../helpers/userHelpers')
var locationHelpers = require('../../helpers/locationHelpers')

var AttendantTest = function () {
  describe('GET /locations/:id/attendants/next', function () {
    it('should get the next available attendant', function (done) {
      locationHelpers.getLocations()
        .then(function (locationResult) {
          var id = locationResult.locations[0]._id
          request(server)
            .get(['/locations', id, 'attendants', 'next'].join('/'))
            .expect(function (res) {
              res.body.should.have.property('name')
              res.body.name.should.have.property('lastname')
            })
            .end(done())
        })
        .catch(function (error) {
          done(error)
        })
    })
  })

// describe('POST /attendants/requests', function () {
//   it('should request to become an attendant', function (done) {
//     // First lets add a dummy key to the cache using the testing endpoints
//     locationHelpers.getLocations()
//       .then(function (locationsResult) {
//         var id = locationsResult.locations[0]._id
//         userHelpers.addUser()
//           .then(function (userResult) {
//             agent
//               .post(['/attendants', 'requests'].join('/'))
//               .set('authorization', 'Bearer ' + userResult.user.access_token)
//               .send({location_id: id})
//               .end(function (err, res) {
//                 if (err) {
//                   done(err)
//                 } else {
//                   res.should.have.property('statusCode', 201)
//                   done()
//                 }
//               })
//           })
//           .catch(function (error) {
//             done(error)
//           })
//       })
//       .catch(function (error) {
//         done(error)
//       })
//   })
// })
}

module.exports = AttendantTest
