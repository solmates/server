'use strict'
var co = require('co')
var dummyjson = require('dummy-json')

var authHelper = require('../../src/helpers/authHelper.js')
var userHelper = require('../../src/helpers/userHelper.js')

module.exports = {
  getOAuthUser: function () {
    return JSON.parse(dummyjson.parse('{"firstname": "{{firstName}}", "lastname": "{{lastName}}", "email": "{{email}}"}'))
  },

  createTestUser: function (user) {
    return new Promise(function (resolve, reject) {
      co(function * () {
        try {
          if (!user) {
            user = this.getOAuthUser()
          }
          var addedUser = yield userHelper.getOrAddUser(user)
          var token = authHelper.createToken()
          yield authHelper.addTokenToUser(token, addedUser._id)
          resolve(userHelper.getUser(token.token))
        } catch (error) {
          reject(error)
        }
      }.bind(this))
    }.bind(this))
  }

}
