/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('GET /regions', function () {
  var nrOfRegions = 2
  it('should have ' + nrOfRegions + ' regions', function (done) {
    request(server)
      .get(basePath + '/regions')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        res.body.should.have.lengthOf(nrOfRegions)
        done()
      })
  })
})
