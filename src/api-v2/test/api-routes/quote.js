/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('POST /quote', function () {
  it('should create a successful quote', function (done) {
    request(server)
      .post(basePath + '/quote')
      .send(require('../data/quote'))
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        console.log(res.error)
        should.not.exist(err)
        done()
      })
  })
})
