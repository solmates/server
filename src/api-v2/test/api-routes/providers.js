/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('GET /providers', function () {
  it('should get a list of all providers', function (done) {
    request(server)
      .get(basePath + '/providers')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(res.body.length, 2)
        done()
      })
  })
})

describe('GET /providers/{LC}/{SKU}', function () {
  it('should get "beachbrothers" as the provider at "duneallen" for "beachset"', function (done) {
    request(server)
      .get(basePath + '/providers/duneallen/beachset')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(res.body.length, 1)
        should.equal(res.body[0].PC, 'beachbrothers')
        done()
      })
  })

  it('should get "beachbrothers" as the provider at "duneallen" for "beachchair" and "beachumbrella"', function (done) {
    request(server)
      .get(basePath + '/providers/duneallen/beachset')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(res.body.length, 1)
        should.equal(res.body[0].PC, 'beachbrothers')
        done()
      })
  })

  it('should get "30ashades" and "beachbrothers" as the provider at "bluemountain" for "beachset"', function (done) {
    request(server)
      .get(basePath + '/providers/bluemountain/beachchair,beachumbrella')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(res.body.length, 2)
        // test needs to be refactored so that order does not matter... maybe use Array.find() to extract the PC from the body?
        should.equal(res.body[0].PC, 'beachbrothers')
        should.equal(res.body[1].PC, '30ashades')
        done()
      })
  })
  it('should 400 with incorrect parameters', function (done) {
    request(server)
      .get(basePath + '/providers/foo/bar')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
      .end(function (err, res) {
        should.exist(err)
        done()
      })
  })
})
