/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath
var version = require('../../package.json').version

describe('GET /version', function () {
  it('should get the version number', function (done) {
    request(server)
      .get(basePath + '/version')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(version, res.body.version)
        done()
      })
  })
})
