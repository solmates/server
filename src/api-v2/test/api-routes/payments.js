/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('POST /payments', function () {
  it('should make a successful payment', function (done) {
    request(server).post(basePath + '/quote').send(require('../data/quote'))
      .end(function (err, res) {
        should.not.exist(err)
        var quoteId = res.body.quoteId
        request(server)
          .post(basePath + '/payments')
          .send({customer: 'cus_8Z9z60CrDBcruu', quoteId: quoteId, guest: require('../data/guest.js')})
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(201)
          .end(function (err, res) {
            should.not.exist(err)
            done()
          })
      })
  })
  it('should fail with a wrong quote id', function (done) {
    request(server)
      .post(basePath + '/payments')
      .send({customer: 'cus_8Z9z60CrDBcruu', quoteId: 'bogusQuoteId'})
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
      .end(function (err, res) {
        should.not.exist(err)
        done()
      })
  })
})
