/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('GET /locations', function () {
  var nrOfLocations = 8
  it('should have ' + nrOfLocations + ' locations', function (done) {
    request(server)
      .get(basePath + '/locations')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        res.body.should.have.lengthOf(nrOfLocations)
        done()
      })
  })
})

describe('GET /locations/{SKU}', function () {
  it('should return a list of locations for "beachset"', function (done) {
    request(server)
      .get(basePath + '/locations/beachset')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        done()
      })
  })
})
