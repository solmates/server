/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var request = require('supertest')
var server = require('../../app')
var basePath = require('../../api-doc').basePath

describe('GET /products', function () {
  var nrOfProducts = 7
  it('should have ' + nrOfProducts + ' products', function (done) {
    request(server)
      .get(basePath + '/products')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        res.body.should.have.lengthOf(nrOfProducts)
        done()
      })
  })
})

describe('GET /products/{LC}', function () {
  it('should get "beachset and bonfire" at LC: "waltondunes"', function (done) {
    request(server)
      .get(basePath + '/products/waltondunes')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err)
        should.equal(res.body[0].SKU, 'beachset')
        should.equal(res.body[1].SKU, 'bonfire')
        done()
      })
  })
})
