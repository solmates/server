/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var co = require('co')
var providerSort = require('../../helpers/providerSort')

describe('providerSort', function () {
  it('should add elements', function (done) {
    co(function * () {
      try {
        yield providerSort.initialize(['a', 'b', 'c'])
        var list = yield providerSort.getCache()
        list.length.should.equal(3)
        list[0].should.equal('c')
        list[1].should.equal('b')
        list[2].should.equal('a')
        done()
      } catch (err) {
        done(err)
      }
    })
  })
  it('should add elements', function (done) {
    co(function * () {
      try {
        yield providerSort.initialize(['a', 'b', 'c'])
        var curProviderC = yield providerSort.roundRobin()
        curProviderC[2].should.equal('c')
        var list = yield providerSort.getCache()
        list.length.should.equal(3)
        list[0].should.equal('b')
        list[1].should.equal('a')
        list[2].should.equal('c')

        // and another roundRobin
        var curProviderB = yield providerSort.roundRobin()
        curProviderB[2].should.equal('b')
        list = yield providerSort.getCache()
        list.length.should.equal(3)

        // and another one
        var curProviderA = yield providerSort.roundRobin()
        curProviderA[2].should.equal('a')
        list = yield providerSort.getCache()
        list.length.should.equal(3)

        // and another one
        var curProviderC2 = yield providerSort.roundRobin()
        curProviderC2[2].should.equal('c')
        list = yield providerSort.getCache()
        list.length.should.equal(3)

        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
