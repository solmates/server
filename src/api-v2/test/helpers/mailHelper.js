c /* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var co = require('co')
var mailHelper = require('../../../api/helpers/mail')

describe('sendMail', function () {
  it('should send email to user', function (done) {
    co(function * () {
      try {
        yield mailHelper({email: 'tjaart@tjaart.co.za'}, 'test email', '<h1>hello world</h1>')
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
