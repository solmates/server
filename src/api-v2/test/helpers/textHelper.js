/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var co = require('co')
var textHelper = require('../../../api/helpers/text')

describe('sendText', function () {
  it('should send text to user', function (done) {
    co(function * () {
      try {
        yield textHelper('+16156680874', 'Hello world')
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})
