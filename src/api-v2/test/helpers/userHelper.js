/* global describe, it */

var should = require('should') // eslint-disable-line no-unused-vars
var co = require('co')
var helperMethods = require('../helperMethods')
var userHelper = require('../../../api/helpers/userHelper')

describe('getOrAddUser(me)', function () {
  it('should add a new user', function (done) {
    co(function * () {
      try {
        var me = {name: {firstname: 'Test', lastname: 'User'}, email: 'test@test.com'}
        var result = yield userHelper.getOrAddUser(me)
        // console.log('result', result)
        done()
      } catch (err) {
        done(err)
      }
    })
  })
})

describe('getUser(token)', function () {
  it('should reject the promise with an  incorrect token', function (done) {
    co(function * () {
      try {
        yield userHelper.getUser('bogus token')
        done(new Error('We expected an error, but found none!'))
      } catch(err) { // eslint-disable-line
        err.message.should.equal('Invalid Token!')
        done()
      }
    })
  })

  it('should return the user', function (done) {
    co(function * () {
      try {
        var user = yield helperMethods.createTestUser()
        user.name.should.have.property('lastname')
        done()
      } catch (error) {
        done(error)
      }
    })
  })
})
