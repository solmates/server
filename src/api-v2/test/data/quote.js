var moment = require('moment')
module.exports = {
  PC: 'beachbrothers',
  LC: 'waltondunes',
  date: moment(),
  products: [{
    SKU: 'beachset',
    quantity: 1,
    duration: 1
  }]
}
