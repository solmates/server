import co from 'co'
import config from 'config'
import authHelper from '../../helpers/auth'
import userHelper from '../../helpers/user'
import logger from '../../helpers/logger'

import stripeFunc from 'stripe'
const stripe = stripeFunc(config.get('stripe.api_key'))

const get = function (req, res) {
  logger.debug('GET /paymentsMethods')
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)
      var cards = yield stripe.customers.listCards(user.stripe)
      res.json(cards.data)
    } catch (err) {
      logger.error(err)
      res.status(500).json({message: err.message})
    }
  })
}

const post = function (req, res) {
  co(function * () {
    try {
      logger.debug('POST /paymentsMethods')
      var token = yield authHelper.getToken(req.headers.authorization)
      logger.debug(token)
      var user = yield userHelper.getUser(token)
      logger.debug(user)
      logger.debug(req)
      var source = yield stripe.customers.createSource(
        user.stripe, {source: req.body.token})
      logger.debug(source)
      res.json(source)
    } catch (err) {
      logger.error(err)
      res.status(500).json({message: err.message})
    }
  })
}

const del = function (req, res) {
  co(function * () {
    try {
      logger.debug('DELETE /payments/methods')
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)
      var stripeId = user.stripe
      var card = req.body.card
      yield stripe.customers.deleteCard(stripeId, card)
      res.status(204).json()
    } catch (err) {
      logger.error(err)
      res.status(500).json({message: err.message})
    }
  })
}

/* TODO: currently we only support updating a payment method to set it as the default. We should probably have a more generic method here!!!*/
const put = function (req, res) {
  co(function * () {
    try {
      logger.debug('PUT /payments/methods')
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)
      var stripeId = user.stripe
      var card = req.body.card
      var updatedCard = yield stripe.customers.update(stripeId, {'default_source': card})
      res.status(200).json(updatedCard)
    } catch (err) {
      logger.error(err)
      res.status(500).json({message: err.message})
    }
  })
}

export { get, post, put, del }

// module.exports.get.apiDoc = {
//   description: 'Get my payment methods',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/paymentMethods'}
//     }
//   }
// }

// module.exports.post.apiDoc = {
//   description: 'Add a new payment methods',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/paymentMethods'}
//     }
//   }
// }

// module.exports.delete.apiDoc = {
//   description: 'Delete a payment methods',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/paymentMethods'}
//     }
//   }
// }

// module.exports.put.apiDoc = {
//   description: 'Update a payment methods',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/paymentMethods'}
//     }
//   }
// }
