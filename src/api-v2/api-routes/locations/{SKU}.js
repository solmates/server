import _ from 'underscore'
import co from 'co'
import logger from '../../../helpers/logger'

import mongoose from '../../../helpers/mongoose'
const Location = mongoose.model('Location')
const Pricelist = mongoose.model('Pricelist')
const Product = mongoose.model('Product')

const get = function (req, res, next) {
  logger.debug('GET /location/{SKU}')
  co(function * () {
    try {
      var SKU = req.params.SKU.split(',')
      var pricelists = yield Pricelist.find({'SKU': {$in: SKU}})

      var locationSet = new Set()
      for (let i in pricelists) {
        for (let j in pricelists[i].locations) {
          locationSet.add(pricelists[i].locations[j].LC)
        }
      }

      var locations = yield Location.find({LC: {$in: Array. from (locationSet)}})

      // This algorithm is the same as the one in locations.js.... maybe we should merge?
      for (let i in locations) {
        var products = []
        var pricelistsLocations = yield Pricelist.find({'locations.LC': locations[i].LC})
        for (let j in pricelistsLocations) {
          var productLocationsArray = _.pluck(pricelistsLocations[j].locations, 'LC')
          if (_.contains(productLocationsArray, locations[i].LC)) {
            var product = yield Product.findOne({SKU: pricelistsLocations[j].SKU})
            if (!product.addon) {
              products.push(pricelistsLocations[j].SKU)
            }
          }
        }
        locations[i]['products'] = _.unique(products)
      }

      logger.debug(locations)

      res.json(locations)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get available location that provide the given product',
//   parameters: [
//     {
//       name: 'SKU',
//       in: 'path',
//       type: 'string',
//       required: true,
//       description: 'The product code'
//     }
//   ],
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/location'
//         }
//       }
//     }
//   }
// }
