'use strict'

import co from 'co'
import logger from '../../../helpers/logger'

import mongoose from '../../../helpers/mongoose'
const Receipt = mongoose.model('Receipt')

const get = function (req, res) {
  co(function * () {
    try {
      var reservationId = req.params.reservationId
      logger.debug('GET /receipts/{reservationId}')
      var receipt = yield Receipt.findOne({reservation: mongoose.Types.ObjectId(reservationId)})
      res.status(200).json(receipt)
    } catch (err) {
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get the receipt for the given reservation',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'object',
//         $ref: '#/definitions/receipt'
//       }
//     }
//   }
// }
