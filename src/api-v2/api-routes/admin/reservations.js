'use strict'

import _ from 'underscore'
import co from 'co'
import logger from '../../../helpers/logger'

import authHelper from '../../../helpers/auth'
import userHelper from '../../../helpers/user'

import mongoose from 'mongoose'
// const Location = mongoose.model('Location')
// const Pricelist = mongoose.model('Pricelist')
const Provider = mongoose.model('Provider')
const Reservation = mongoose.model('Reservation')

const get = function (req, res) {
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      let user = yield userHelper.getUser(token)
      if (token) {
        console.log(user)
        let providers = yield Provider.find({'admins.email': user.email})
        var PCs = _.pluck(providers, 'PC')
        var reservations = yield Reservation.find({'invoice.PC': {$in: PCs}})
        res.status(200).json(reservations)
      } else {
        res.status(401).json({message: 'Access Token required.'})
      }
    } catch (err) {
      logger.error(err.message)
      res.status(500).send(err.message).end()
    }
  })
}
export { get }
