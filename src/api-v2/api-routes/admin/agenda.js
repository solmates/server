/* global emit */

import authHelper from '../../../helpers/auth'
import userHelper from '../../../helpers/user'
import co from 'co'
import moment from 'moment'
import logger from '../../../helpers/logger'
import mongoose from 'mongoose'
const Reservation = mongoose.model('Reservation')

const get = function (req, res) {
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      if (token) {
        var user = yield userHelper.getUser(token)
        var today = moment().startOf('day')
        var agenda = yield getAgenda()
        var result = yield agenda.find()
        res.json(result)
      } else {
        res.status(401).json({message: 'Access Token required.'})
      }
    } catch (err) {
      logger.error(err)
      res.status(500).json({message: err})
    }
  })
}

export { get }

var getAgenda = function () {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      try {
        var today = moment().startOf('day')
        // First we get totals per location, per date, per item
        var mapReduce = yield Reservation.mapReduce({
          query: {'invoice.date': {$gte: today}},
          map: function () {
            var date = this.invoice.date.toLocaleDateString()
            var key = {date: date, LC: this.invoice.LC}
            for (var i in this.invoice.products) {
              var value = {
                LC: this.invoice.LC,
                date: date,
                SKU: this.invoice.products[i].SKU,
                quantity: this.invoice.products[i].quantity,
                reservation: this._id.str
              }
              emit(key, value)
            }
          },
          reduce: function (id, values) {
            var reducedObject = {}
            reducedObject.reservations = []

            reducedObject.totals = []
            var productsList = []
            values.forEach(function (value) {
              if (productsList.indexOf(value.SKU) < 0) {
                productsList.push(value.SKU)
                reducedObject.totals.push({product: value.SKU, quantity: value.quantity})
              } else {
                reducedObject.totals.forEach(function (total) {
                  if (total.SKU === value.SKU) {
                    total.quantity += value.quantity
                  }
                })
              }
            })

            values.forEach(function (value) {
              reducedObject.location = value.LC
              reducedObject.date = value.date

              // don't add duplicates
              if (reducedObject.reservations.indexOf(value.reservation) < 0) {
                reducedObject.reservations.push(value.reservation)
              }
            }
            )
            return reducedObject
          },
          out: { replace: 'totalperlocations' },
          verbose: true
        })
        resolve(mapReduce)
      } catch (err) {
        reject(err)
      }
    })
  })
}
