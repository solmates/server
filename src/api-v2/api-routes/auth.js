'use strict'

import co from 'co'
import logger from '../../helpers/logger'
import authHelper from '../../helpers/auth'
import userHelper from '../../helpers/user'

import mongoose from '../../helpers/mongoose'
const User = mongoose.model('User')

const post = function (req, res) {
  co(function * () {
    try {
      logger.debug('POST /auth')
      var oauthToken = req.body.token
      var provider = req.body.provider
      logger.debug(oauthToken)

      var userJson = yield authHelper.auth(oauthToken, provider)
      logger.debug(userJson)
      var user = yield userHelper.getOrAddUser(userJson)
      // Add a new token for the user
      var token = authHelper.createToken()
      yield authHelper.addTokenToUser(token, user._id)

      let leanJson = User.findById(user._id).lean().exec()
      res.status(200).json(Object.assign(leanJson, {token: token.token}))
    } catch (err) {
      logger.error(err)
      res.status(400).json({message: err.message}).end()
    }
  })
}

export { post }
// module.exports.post.apiDoc = {
//   description: 'Authenticates the user',
//   parameters: [
//     {
//       name: 'auth',
//       in: 'body',
//       description: 'The token obtained from the requested provider',
//       schema: { $ref: '#/definitions/auth' },
//       required: true
//     }
//   ],
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array'
//       }
//     },
//     400: { description: 'Bad request' },
//     default: { description: 'Internal server error' }

//   }
// }
