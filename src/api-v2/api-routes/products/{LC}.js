import co from 'co'
import logger from '../../../helpers/logger'

import mongoose from '../../../helpers/mongoose'
const Product = mongoose.model('Product')
const Pricelist = mongoose.model('Pricelist')

const get = function (req, res, next) {
  logger.debug('GET /products/{LC}')
  co(function * () {
    try {
      var LC = req.params.LC.split(',')
      logger.debug(LC)
      var productIds = yield Pricelist.find({'locations.LC': {$in: LC}}, {SKU: 1})
      logger.debug(productIds)

      var productSet = new Set()
      for (var i in productIds) {
        productSet.add(productIds[i].SKU)
      }

      var products = yield Product.find({SKU: {$in: Array. from (productSet)},
      addon: {$not: {$eq: true}}})

      logger.debug(products)

      res.json(products)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get available products at a given location',
//   parameters: [
//     {
//       name: 'LC',
//       in: 'path',
//       type: 'string',
//       required: true,
//       description: 'The location to find products'
//     }
//   ],
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/provider'
//         }
//       }
//     }
//   }
// }
