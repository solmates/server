import co from 'co'
import logger from '../../../helpers/logger'
import updateQuote from '../../../helpers/quote'
import quoteCache from '../../../helpers/quoteCache'

const get = function (req, res, next) {
  co(function * () {
    try {
      logger.debug('GET /quote/{{quoteId}}')
      var quoteId = req.params.quoteId
      var quote = yield quoteCache.get(quoteId)
      if (!quote) {
        res.status(400).send({message: 'Quote not found!'}).end()
      } else {
        res.status(200).json(quote)
      }
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

const put = function (req, res, next) {
  co(function * () {
    try {
      var quoteId = req.params.quoteId
      var quoteRequest = req.body
      var quoteResponse = yield updateQuote(quoteRequest, quoteId)
      res.status(200).json(quoteResponse)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get, put }
