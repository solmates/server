import _ from 'underscore'
import co from 'co'
import logger from '../../../../helpers/logger'
import ProviderSort from '../../../../helpers/providerSort'

import mongoose from '../../../../helpers/mongoose'
const Pricelist = mongoose.model('Pricelist')
const Provider = mongoose.model('Provider')

co(function * () {
  var providers = yield Provider.find({})
  logger.debug(_.pluck(providers, 'PC'))
  yield ProviderSort.initialize(_.pluck(providers, 'PC'))
  logger.debug(yield ProviderSort.getCache())
})

// TODO: Currently this will return providers that stock any of the products (if more than one is defined). Maybe we should limit it to providers that stock ALL products?
const get = function (req, res, next) {
  co(function * () {
    logger.debug('GET /provider')
    var providerSet = new Set()
    try {
      var SKUs = req.params.SKU.split(',')
      var LC = req.params.LC

      // find the pricelist entries for the given products at the given location
      var pricelists = yield Pricelist.find({SKU: {$in: SKUs}, 'locations.LC': {$in: LC}})

      for (var i in pricelists) {
        providerSet.add(pricelists[i].PC)
      }

      var providers = yield Provider.find({PC: {$in: Array. from (providerSet)}})

      for (let j in pricelists) {
        for (let k in providers) {
          if (pricelists[j].PC === providers[k].PC) {
            if (pricelists[j].minTime) { providers[k]._doc.minTime = pricelists[j].minTime }
            if (pricelists[j].maxTime) { providers[k]._doc.maxTime = pricelists[j].maxTime }
            logger.debug(providers[k])
          }
        }
      }

      logger.debug(ProviderSort.getCache())
      var PCs = _.pluck(providers, 'PC')
      var roundRobinArray = yield ProviderSort.roundRobin(PCs)
      logger.debug(roundRobinArray)
      var sortedProviders = []
      for (let k in roundRobinArray) {
        var j = PCs.indexOf(roundRobinArray[k])
        logger.debug(j)
        if (j >= 0) {
          logger.debug(providers[j])
          sortedProviders.push(providers[j])
        }
      }

      logger.debug(sortedProviders)
      res.json(sortedProviders)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}
export { get }

// module.exports.get.apiDoc = {
//   description: 'Get available providers',
//   parameters: [
//     {
//       name: 'LC',
//       in: 'path',
//       type: 'string',
//       required: true,
//       description: 'The Location Code where the provider operates'
//     },
//     {
//       name: 'SKU',
//       in: 'path',
//       type: 'string',
//       required: true,
//       description: 'The Stock Keeping Unit (product code) for a product the provider supplies'
//     }
//   ],
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/provider'
//         }
//       }
//     }
//   }
// }
