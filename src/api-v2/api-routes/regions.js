import co from 'co'
import logger from '../../helpers/logger'
import mongoose from '../../helpers/mongoose'
const Region = mongoose.model('Region')

const get = function (req, res) {
  co(function * () {
    try {
      var regions = yield Region.find()
      res.json(regions)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get the regions',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/region'
//         }
//       }
//     }
//   }
// }
