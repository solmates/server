import _ from 'underscore'
import co from 'co'
import config from 'config'
import logger from '../../helpers/logger'
import mail from '../../helpers/mail'
import text from '../../helpers/text'
import generateReceipt from '../../helpers/receipt'
import authHelper from '../../helpers/auth'
import userHelper from '../../helpers/user'
import quoteCache from '../../helpers/quoteCache'

import stripeFunc from 'stripe'
const stripe = stripeFunc(config.get('stripe.api_key'))

import mongoose from '../../helpers/mongoose'
const Location = mongoose.model('Location')
const Payment = mongoose.model('Payment')
const Product = mongoose.model('Product')
const Provider = mongoose.model('Provider')
const Receipt = mongoose.model('Receipt')
const Reservation = mongoose.model('Reservation')

const post = function (req, res) {
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      if (token) {
        var user = yield userHelper.getUser(token)
      }

      var tipPercent = req.body.tipPercent
      var quoteId = req.body.quoteId
      var guest = req.body.guest
      var source = req.body.source
      var customer = req.body.customer

      // Retrieve the quote from the cache
      logger.debug(quoteId)

      logger.debug(req)
      var quote = yield quoteCache.get(quoteId)
      logger.debug(quote)
      if (!quote) {
        res.status(400).json({message: 'Invalid quote id'}).end()
      }

      // Create the reservation
      if (!tipPercent) { tipPercent = 0 }

      var total = quote.total
      var tip = tipPercent * total

      var reservationParams = {guest: guest, invoice: quote, tip: tip}
      if (user) { Object.assign(reservationParams, {user: {email: user.email}}) }
      var reservation = yield createReservation(reservationParams)

      // Make the payment
      var charge = yield pay({
        total: total + tip,
        reservationId: reservation._id.toString(),
        customer: customer,
        source: source
      })
      // Persist the payment results
      var paymentInstance = new Payment(charge)
      var payment = yield paymentInstance.save()

      // Create a receipt
      var location = yield Location.findOne({LC: reservation.invoice.LC})
      var provider = yield Provider.findOne({PC: reservation.invoice.PC})
      var products = yield Product.find({SKU: {$in: _.pluck(reservation.invoice.products, 'SKU')}})
      var receipt = yield generateReceipt({
        location: location,
        reservation: reservation,
        products: products,
        payment: payment,
        provider: provider
      })

      var receiptDoc = {reservation: reservation._id, receipt: receipt.html, user: reservation.user}
      // Persist the receipt
      var receiptInstance = new Receipt(receiptDoc)
      yield receiptInstance.save()

      // Mail the receipts. Currently we mail the guest and CC the provider and support
      yield mailReceipt({to: [reservation.guest.email],
        cc: [provider.contact.email, 'support@getsolmates.com'],
      receipt: receipt.html})

      // text the provider
      yield textReceipt({to: provider.contact.mobile, receipt: receipt.short})

      logger.debug('got here')

      res.status(201).send(charge).end()
    } catch (err) {
      logger.error(err)
      res.status(400).send({message: err.message}).end()
    }
  })
}

export { post }

// module.exports.post.apiDoc = {
//   description: 'Make a payment',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/paymentMethods'}
//     }
//   }
// }

var createReservation = function (params) {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      try {
        var reservationInstance = new Reservation(params)
        logger.debug(reservationInstance)
        var reservation = yield reservationInstance.save()
        resolve(reservation)
      } catch (err) {
        logger.error(err)
        reject(err)
      }
    })
  })
}

var pay = function (options) {
  return new Promise(function (resolve, reject) {
    // configure the payment parameters
    var params = {
      amount: options.total,
      currency: 'usd',
      description: 'Solmates LLC',
      metadata: {
        reservation_id: options.reservationId
      }
    }

    if (options.source) {
      Object.assign(params, {source: options.source})
    } else if (options.customer) {
      Object.assign(params, {customer: options.customer})
    } else {
      reject(new Error('No payment method found'))
    }

    stripe.charges.create(params, {idempotency_key: options.reservationId},
      function (err, charge) {
        if (err) {
          logger.error(err)
          reject(err)
        }
        resolve(charge)
      })
  })
}

var mailReceipt = function (params) {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      try {
        // Mail the guest
        yield mail(params.address, 'SOLMATES Receipt', params.receipt)
        resolve()
      } catch (err) {
        logger.error(err)
        reject(err)
      }
    })
  })
}

var textReceipt = function (params) {
  return new Promise(function promise (resolve, reject) {
    co(function * () {
      try {
        if (params.to) {
          yield text(params.to, params.receipt)
        }
        resolve()
      } catch (err) {
        logger.error(err)
        reject(err)
      }
    })
  })
}
