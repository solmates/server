'use strict'

import packageInfo from '../../../package'

const get = function (req, res) {
  res.json({version: packageInfo.version})
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get the current server version',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/reservation'
//         }
//       }
//     }
//   }
// }
