import _ from 'underscore'
import co from 'co'
import logger from '../../helpers/logger'

import mongoose from '../../helpers/mongoose'
const Product = mongoose.model('Product')

const get = function (req, res) {
  const apiVersion = req.url.split('/')[1]
  if (apiVersion === 'v2') {
    get20(req, res)
  } else {
    get21(req, res)
  }
}

export { get }

const get20 = function (req, res) {
  co(function * () {
    try {
      var products = yield Product.find()
      res.json(products)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

const get21 = function (req, res) {
  co(function * () {
    try {
      var products = yield Product.find()
      // TODO: Refactor. Not great code. We nest sub-products, as it is difficult to nest the products on the client side
      _.each(products, function (product) {
        var SKUs = _.pluck(product.children, 'SKU')
        if (product.children && product.children.length > 0) {
          var foundProducts = _.filter(products, function (childProduct) {
            if (_.contains(SKUs, childProduct.SKU)) {
              return true
            }
          })
          _.each(foundProducts, function (foundProduct) {
            var i = _.indexOf(SKUs, foundProduct.SKU)
            var quantity = product.children[i].quantity
            Object.assign(product.children[i], foundProduct, {quantity: quantity})
          })
        }
      })
      res.json(products)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}
