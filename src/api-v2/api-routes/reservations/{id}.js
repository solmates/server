'use strict'

import co from 'co'
import logger from '../../../helpers/logger'

import mongoose from '../../../helpers/mongoose'
const Reservation = mongoose.model('Reservation')

const get = function (req, res) {
  co(function * () {
    try {
      logger.debug('GET /reservations')
      var id = req.params.id
      var reservations = yield Reservation.find({_id: id})
      res.status(200).json(reservations)
    } catch (err) {
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   parameters: [{
//     name: 'id',
//     in: 'path',
//     type: 'string',
//     required: true,
//     description: 'The reservation id for the required reservation.'
//   }],

//   description: 'Get the reservation for the for the given id',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/reservation'
//         }
//       }
//     }
//   }
// }
