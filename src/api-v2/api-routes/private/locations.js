import _ from 'underscore'
import co from 'co'
import logger from '../../../helpers/logger'
import authHelper from '../../../helpers/auth'
import userHelper from '../../../helpers/user'
// import logger from '../../helpers/logger'

import mongoose from '../../../helpers/mongoose'
const OneTimePin = mongoose.model('OneTimePin')
const PrivateLocation = mongoose.model('PrivateLocation')
const User = mongoose.model('User')

const post = function (req, res, next) {
  co(function * () {
    try {
      console.log(req.body.key)
      var key = req.body.key
      var pin = yield OneTimePin.findOne({key: key})

      console.log(pin)
      if (!pin) {
        res.status(400).send({message: 'Invalid pin number'}).end()
      }

      var token = yield authHelper.getToken(req.headers.authorization)
      console.log(req.headers)
      console.log(token)
      var user = yield userHelper.getUser(token)

      var userInstance = User.findOne({_id: user._id})
      var privateLocation = yield PrivateLocation.find({LC: pin.LC})
      if (privateLocation) {
        user.privateZones.push(pin)
        yield userInstance.update(user)
        yield OneTimePin.findOneAndRemove({_id: pin._id})
      }

      res.json(privateLocation)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

const get = function (req, res, next) {
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)

      var LCs = []
      var now = new Date()
      _.each(user.privateZones, function (zone) {
        if (now < zone.date) {
          LCs.push(zone.LC)
        }
      })

      LCs = _.uniq(LCs)
      var privateLocations = yield PrivateLocation.find({LC: {$in: LCs}})
      res.json(privateLocations)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get, post }
