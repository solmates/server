import _ from 'underscore'
import co from 'co'
import logger from '../../../../helpers/logger'
import authHelper from '../../../../helpers/auth'
import userHelper from '../../../../helpers/user'

import mongoose from '../../../../helpers/mongoose'
const PrivateLocation = mongoose.model('PrivateLocation')

const get = function (req, res, next) {
  co(function * () {
    try {
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)

      var LCs = req.params.LC.split(',')
      _.each(user.privateZones, function (zone) {
        if (_.contains(LCs, zone.LC)) {
          LCs.push(zone.LC)
        }
      })
      LCs = _.uniq(LCs)
      var privateLocations = yield PrivateLocation.find({LC: {$in: LCs}})
      res.json(privateLocations)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}
export { get }
