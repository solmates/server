import _ from 'underscore'
import co from 'co'
import logger from '../../helpers/logger'

import mongoose from '../../helpers/mongoose'
const Location = mongoose.model('Location')
const Pricelist = mongoose.model('Pricelist')
const Product = mongoose.model('Product')

const get = function (req, res, next) {
  co(function * () {
    logger.debug('location')
    try {
      var locations = yield Location.find()

      // This algorithm is the same as the one in {SKU}/locations.js.... maybe we should merge?
      // calculate all the products available at the current location
      for (var i in locations) {
        // var products = []
        var pricelists = yield Pricelist.find({'locations.LC': locations[i].LC})
        var productsSKUs = []
        for (var j in pricelists) {
          var productLocationsArray = _.pluck(pricelists[j].locations, 'LC')
          if (_.contains(productLocationsArray, locations[i].LC)) {
            productsSKUs.push(pricelists[j].SKU)
          }
        }
        productsSKUs = _.unique(productsSKUs)
        var productResults = yield Product.find({$and: [{SKU: {$in: productsSKUs}}, {addon: {$not: {$eq: true}}}]})
        var products = _.pluck(productResults, 'SKU')
        locations[i]['products'] = _.unique(products)
      }

      res.json(locations)
    } catch (err) {
      logger.error(err)
      res.status(500).send(err).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Returns all locations',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/locationResponse'
//         }
//       }
//     }
//   }
// }
