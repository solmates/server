import co from 'co'
import logger from '../../helpers/logger'
import updateQuote from '../../helpers/quote'

const post = function (req, res, next) {
  co(function * () {
    try {
      logger.debug('POST /quote')
      var quoteRequest = req.body
      var quoteResponse = yield updateQuote(quoteRequest)
      res.status(200).json(quoteResponse)
    } catch (err) {
      logger.debug(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { post }
