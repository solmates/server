import co from 'co'
import logger from '../../helpers/logger'

import mongoose from '../../helpers/mongoose'
const Provider = mongoose.model('Provider')

const get = function (req, res, next) {
  co(function * () {
    logger.debug('GET /provider')
    try {
      var providers = yield Provider.find({})
      res.json(providers)
    } catch (err) {
      logger.error(err)
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get all providers',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'array',
//         items: {
//           $ref: '#/definitions/provider'
//         }
//       }
//     }
//   }
// }
