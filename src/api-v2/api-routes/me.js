import co from 'co'
import authHelper from '../../helpers/auth'
import userHelper from '../../helpers/user'
import logger from '../../helpers/logger'

const get = function (req, res) {
  co(function * () {
    try {
      logger.debug('GET /me')
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)
      res.status(200).json(user)
    } catch (err) {
      res.status(500).json({message: err.message})
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Returns me',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {$ref: '#/definitions/guest'}
//     }
//   }
// }
