'use strict'

import authHelper from '../../helpers/auth'
import userHelper from '../../helpers/user'
import co from 'co'
import logger from '../../helpers/logger'

import mongoose from '../../helpers/mongoose'
var Reservation = mongoose.model('Reservation')

const get = function (req, res) {
  co(function * () {
    try {
      logger.debug('GET /reservations')
      var token = yield authHelper.getToken(req.headers.authorization)
      var user = yield userHelper.getUser(token)
      var reservations = yield Reservation.find({user: user._id.toString()})
      res.status(200).json(reservations)
    } catch (err) {
      res.status(500).send({message: err.message}).end()
    }
  })
}

export { get }

// module.exports.get.apiDoc = {
//   description: 'Get all reservations for the current user',
//   responses: {
//     200: {
//       description: 'Success',
//       schema: {
//         type: 'object',
//         $ref: '#/definitions/reservation'
//       }
//     }
//   }
// }
