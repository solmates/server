FROM alpine:3.4
MAINTAINER Tjaart van der Walt <tjaart@getsolmatesapp.com>
ARG NPM_TOKEN="f1487558-e22e-487c-8407-f0385ec65a52"
WORKDIR solmates
COPY . .

# install nodejs
RUN apk update && \
    apk add --no-cache nodejs

# install dependencies
RUN echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > ~/.npmrc && \
    npm install && rm ~/.npmrc

CMD ["npm", "start"]
