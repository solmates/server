# Configuring a dev environment #

## Prerequisites ##

- Nodejs installed
- Access to the `solmatesapp` Bitbucket team
- Access to the `@solmates` private npm repos.

(email [tjaart@tjaart.co.za](mailto:tjaart@tjaart.co.za) if you require access)

## Configuration ##

### Step 1 ###

Clone `solmates-server`, and install its dependencies

```shell
git clone git@bitbucket.org:solmatesapp/solmates-server.git
cd solmates-server
npm install
```

Thats it, there is no step 2 :)


## Running the server ##

The configuration used by the server is determined by the `NODE_ENV` environment variable.

The following 3 environments are defined:

- production
- test
- development (default)

to start the server run:

```shell
npm start
```

### API documentation ###
You can peruse the API documentation, using `swagger-ui`, by running:

```shell
npm run start-swagger-ui
```

Unfortunately this opens the Swagger Petstore Example API by default. To see the Solmates API you need to enter the correct server URL and click explore.

The url should be something to the effect of: `http://localhost:3000/swagger`


[co](https://github.com/tj/co)
