import config from 'config'

var host, port, db, user, password
config.has('dbConfig.host') ? host = config.get('dbConfig.host') : host = ''
config.has('dbConfig.port') ? port = parseInt(config.get('dbConfig.port')) : port = ''
config.has('dbConfig.dbName') ? db = config.get('dbConfig.dbName') : db = ''
config.has('dbConfig.username') ? user = config.get('dbConfig.username') : user = ''
config.has('dbConfig.password') ? password = config.get('dbConfig.password') : password = ''

module.exports = {
  host: host,
  port: port,
  db: db,
  user: user,
  password: password,
  collection: '_migrations',
  directory: 'database/migrations',
  poolSize: 2
}
