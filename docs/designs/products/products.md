# Solmates Products Design #

In our original design, we have both `product`s and `item`s. For version 2 we deprecate `item`s and now we have only `product`s.

## Goal ##

In the original design, `product`s were only a place holder for a collections of item types. The number of each type of item is defined in the reservation. For our new design we wanted to give  `product` a stricter definition which includes number of items, while still maintaining the flexibility of being able to add additional `product`s.

## Overview ##

We achieve the structure part by having a `children` field on a `product` which allows for nested products.
For example an extract from a beach set product might look like this:

```json
{
  "children": [
    {
      "SKU": "beachchair",
      "quantity": 2
    },
    {
      "SKU": "beachumbrella",
      "quantity": 1
    }
  ]
}

```

We achieve the flexibility we need by having a list of `products` in a `reservation`. This way we can still build up arbitrarily complicated reservations by combining existing products. For example if we want to build a 3 Chair, 2 Umbrella beach set, the `reservation` would look like this:

```json
{
  "products": [
    {
      "SKU": "beachchset",
      "quantity": 1
    },
    {
      "SKU": "beachchchair",
      "quantity": 1
    },  
  ]
}
```

## Addon products ##

Now that `item`s have been deprecated, we need to limit certain products so that they cannot be sold as individual products, but rather only form part of another product, or addition to another product. For these products we add a boolean `addon` field

```json
{
  addon: true
}
```
