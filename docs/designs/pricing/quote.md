# Solmates Quote Design #

## Goal ##

With our new [Pricing Design](./pricing.md) we calculate a reservation price server side. We now need a way to communicate this price to the client. 

It should have the following features:
- Guarantee the price for a reservation for `x` amount of time.
- Protect the server from processing a payment for a modified reservation.

## Overiew ##

The best way to ensure that a reservation remains unmodified is to take a checksum of the request, and use that as the `quote_id`.

## Open issues ##

At this time it is unclear on exactly which part of the reservation do we use in the checksum? If it is the entire reservation any trivial changes will result in a new quote being produced. (I can think of for example a change in tip, or email address) On the other hand it seems that a quote should be set in stone, and any changes will produce a new quote.

Should quotes be persisted? Especially if any changes results in new quotes, persisting quotes can become expensive.
