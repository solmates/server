# Solmates Pricing Design#

## Goals ##

We want to redesign the pricing to support more advanced pricing schemes. This needs to be done with an eye on being able to do dynamic pricing in the future.

The new scheme should support:

- multiple providers per beach zone
- customized pricing per provider per product
- as far as possible, pricing should reflect the provider's current pricing model

## Design overview##

Our old (undocumented) design calculates the price client side based on `item`  information. Once you pay for the reservation we would recalculate the price server side, and use this server calculated price. Not a great design.

It became clear in our discussion that price needs to be calculated server side, and communicated it to the client, guaranteeing the price for a period of time. This allows for arbitrarily complex pricing without any upgrade to the client, and is also the only way to achieve dynamic pricing.

## Diagram ##

![Alt text](./pricing.svg)
